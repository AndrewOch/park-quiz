package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.*;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {

        List<ParkParsingException.ParkValidationError> list = new ArrayList<>();
        try {
            File file = new File(parkDatafilePath);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String s;

            Map<String, String> hashMap = new HashMap<>();
            while ((s = bufferedReader.readLine()) != null) {
                if (s.contains("legalName")) {
                    String substring = s.substring(s.indexOf(':') + 2);
                    String legalName = substring.replaceAll("\"", "");
                    if (legalName.length() == 0) {
                        legalName = null;
                    }
                    hashMap.put("legalName", legalName);
                }
                if (s.contains("ownerOrganizationInn")) {
                    String ownerOrganizationInn = s.substring(s.indexOf(':') + 2).replaceAll("\"", "");
                    if (ownerOrganizationInn.length() == 0) {
                        ownerOrganizationInn = null;
                    }
                    hashMap.put("ownerOrganizationInn", ownerOrganizationInn);
                }
                if (s.contains("foundationYear")) {
                    String foundationYear = s.substring(s.indexOf(':') + 2).replaceAll("\"", "");
                    if (foundationYear.length() == 0) {
                        foundationYear = null;
                    }
                    hashMap.put("foundationYear", foundationYear);
                }
                if (s.contains("title")) {
                    String title = s.substring(s.indexOf(':') + 2).replaceAll("\"", "");
                    if (title.length() == 0) {
                        title = null;
                    }
                    hashMap.put("title", title);
                }
            }
            bufferedReader.close();

            Class<?> aClass = Park.class;
            Constructor<?> declaredConstructor = aClass.getDeclaredConstructor();
            declaredConstructor.setAccessible(true);
            Park park = (Park) declaredConstructor.newInstance();
            Field[] declaredFields = aClass.getDeclaredFields();
            Arrays.stream(declaredFields).forEach(field -> {

                field.setAccessible(true);
                FieldName fieldName = field.getDeclaredAnnotation(FieldName.class);
                if (fieldName != null) {
                    try {
                        field.set(park, hashMap.get(fieldName.value()));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                NotBlank notBlank = field.getDeclaredAnnotation(NotBlank.class);

                if (notBlank != null) {
                    try {
                        if (hashMap.get(field.getName()) == null) {
                            list.add(new ParkParsingException.ParkValidationError(field.getName(), "Поле не должно быть пустым!"));
                        } else {
                            if (field.getType() == LocalDate.class) {
                                field.set(park, LocalDate.parse(hashMap.get(field.getName())));
                            } else {
                                field.set(park, hashMap.get(field.getName()));
                            }
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                MaxLength maxLength = field.getDeclaredAnnotation(MaxLength.class);

                if (maxLength != null) {
                    try {
                        if (hashMap.get(field.getName()).length() > maxLength.value()) {
                            list.add(new ParkParsingException.ParkValidationError(field.getName(), "Поле не должно иметь длину больше: " + maxLength.value()));
                        } else {
                            field.set(park, hashMap.get(field.getName()));
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }

            });
            if (list.size() > 0) {
                throw new ParkParsingException("Ошибки валидации", list);
            } else
                return park;
        } catch
        (IOException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException
                        e) {
            e.printStackTrace();
        }
        return null;
    }
}
